<?php

class Paginas extends Controller
{

    public function __construct()
    {
        // Desde aquí cargaremos los modelos.
    }

    public function index(){
        $data = [
            "Titulo" => "Framework de Manuel Mañas Alfaro"
        ];
        $this->view('Paginas/index',$data);
    }

}
